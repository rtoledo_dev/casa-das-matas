$(function(){
	if($('.pro_tweet').length)$.include('/assets/template/jquery.tweet.js');
	// if($('.lightbox-image').length)$.include('/assets/template/jquery.prettyPhoto.js');
	if($('.pro_kwicks').length)$.include('/assets/template/kwicks-1.5.1.pack.js');
	if($('#pro_counter').length)$.include('/assets/template/jquery.countdown.js');
	if($('.fixedtip').length||$('.clicktip').length||$('.normaltip').length)$.include('/assets/template/jquery.atooltip.pack.js')
// Code
	$("pre.pro_htmlCode2").snippet("html",{style:"print",showNum:false,menu:false});
	$("pre.pro_jsCode2").snippet("javascript",{style:"print",showNum:false,menu:false});
// SlideDown
	$(".pro_description-box dd").show()
	$("pre.pro_htmlCode").snippet("html",{style:"print"});			
	$("pre.pro_cssCode").snippet("css",{style:"print"});			
	$("pre.pro_jsCode").snippet("javascript",{style:"print"});
	$(".pro_description-box dd").hide()	
	$(".pro_description-box dt").click(function(){
		$(this).toggleClass("active").parent(".pro_description-box").find("dd").slideToggle(400);					
	});
	$(".pro_slide-down-box dt").click(function(){$(this).toggleClass("active").parent(".pro_slide-down-box").find("dd").slideToggle(200);});
	$(".pro_slide-down-box2 dt").click(function(){$(this).toggleClass("active").parent(".pro_slide-down-box2").find("dd").slideToggle(200);});	
// Tabs
	$(".pro_tabs1 ul").tabs(".pro_tabs1 .pro_tab-content");
	$(".pro_tabs2 ul").tabs(".pro_tabs2 .pro_tab-content");
	$(".pro_tabs3 ul").tabs(".pro_tabs3 .pro_tab-content");
	$(".pro_tabs4 ul").tabs(".pro_tabs4 .pro_tab-content");
	$(".pro_tabs5 ul").tabs(".pro_tabs5 .pro_tab-content");
	$(".pro_tabs-horz-top ul.pro_tabs-nav").tabs(".pro_tabs-horz-top .pro_tab-content");
	$(".pro_tabs-horz-bottom ul.pro_tabs-nav").tabs(".pro_tabs-horz-bottom .pro_tab-content");
	$(".pro_tabs-horz-top2 ul.pro_tabs-nav").tabs(".pro_tabs-horz-top2 .pro_tab-content");
	$(".pro_tabs-horz-bottom2 ul.pro_tabs-nav").tabs(".pro_tabs-horz-bottom2 .pro_tab-content");
	$(".pro_tabs-vert-left ul.pro_tabs-nav").tabs(".pro_tabs-vert-left .pro_tab-content");
	$(".pro_tabs-vert-right ul.pro_tabs-nav").tabs(".pro_tabs-vert-right .pro_tab-content");	
// Forms
	$('#pro_form2').jqTransform({imgPath:'/assets/template/'});
// Carausel
	$('.pro_list-car').uCarousel({show:4,buttonClass:'pro_car-button', pageStep:1, shift:false})
	$('.pro_carousel').uCarousel({show:4,buttonClass:'pro_car-button'})
	$('.carousel').uCarousel({show:1,pageStep:1,buttonClass:'car-button'})
// Slider
	$('.pro_slider')._TMS({
		show:0,
		pauseOnHover:false,
		prevBu:'.pro_prev',
		nextBu:'.pro_next',
		playBu:'.pro_play',
		items:'.pro_items>li',
		duration:1000,
		preset:'simpleFade',
		bannerCl:'pro_banner',
		numStatusCl:'pro_numStatus',
		pauseCl:'pro_paused',
		pagination:true,
		paginationCl:'pro_pagination',
		pagNums:false,
		slideshow:7000,
		numStatus:true,
		banners:'fade',// fromLeft, fromRight, fromTop, fromBottom
		waitBannerAnimation:false,
		progressBar:'<div class="pro_progbar"></div>'})	
// Simple Gallery
	$('.pro_simple_gallery')._TMS({
			show:0,
			pauseOnHover:true,
			prevBu:false,
			nextBu:false,
			playBu:false,
			pagNums:false,
			numStatus:false,
			duration:1000,
			preset:'simpleFade',
			items:'.pro_items>li',
			bannerCl:'pro_banner',
			pagination:$('.pro_img-pags').uCarousel({show:10,shift:0,buttonClass:'pro_btn'}),
			paginationCl:'pro_gal-pags',
			slideshow:5000,
			banners:'fade',// fromLeft, fromRight, fromTop, fromBottom
			waitBannerAnimation:false,
			progressBar:'<div class="pro_progbar"></div>'})		
// Ranges	
$("#pro_font-size-slider").change(function(e) {$(".pro_icons.pro_basic li a").css("font-size", $(this).val() + "px");});
$(".pro_color-slider").change(function(e) {$(".pro_icons.pro_basic li a").css("color", "hsla(" + $("#pro_color-slider-1").val() + ", " + $("#pro_color-slider-2").val() + "%, " + $("#pro_color-slider-3").val() + "%, 1)");	});
$(".pro_shadow-slider").change(function(e) {	$(".pro_icons.pro_basic li a").css("text-shadow", $("#pro_shadow-slider-1").val() + "px " + $("#pro_shadow-slider-2").val() + "px " + $("#pro_shadow-slider-3").val() + "px black");	 });
// Testimonials
	$('#pro_testimonials').cycle({fx:'fade', height:'auto',timeout:0,next:'#pro_next_testim',prev:'#pro_prev_testim', after: onAfter });
// Buttons
	$(".pro_notClicked").click(function(event) {event.preventDefault();});
})
// Panel
$(function(){
	// Slider
	$('.slider')._TMS({
		show:0,
		pauseOnHover:false,
		prevBu:'.prev',
		nextBu:'.next',
		duration:10000,
		preset:'zoomer',
		bannerCl:'banner',
		pagination:false,
		pagNums:false,
		slideshow:7000,
		numStatus:false,
		banners:'fade',// fromLeft, fromRight, fromTop, fromBottom
		waitBannerAnimation:false})	
		// $('.page1-box1').append('<span class="heading-after"></span><span class="heading-before"></span>')
// 		$(window).load(function(){
//  $('.heading-after').each(function() {
//        var thiswidth = $(this).parent().width() - $(this).prev().width() - 10;
//     $(this).css({width:thiswidth})
//     });

//  $('.page1-box1').each(function() {
//        var thiswidthcenter = ($(this).width() - $(this).find('.heading').width())/2 - 43;
//     $(this).find('.heading-after').css({width: thiswidthcenter})
//     $(this).find('.heading-before').css({width: thiswidthcenter})
//     });
// })	
			$('.pager').cycle({ 
			fx:      'fade',
			timeout:  0,
			speed:    2000,
			pager: '#nav'
		});	
});
function onAfter(curr, next, opts, fwd){var $ht=$(this).height();$(this).parent().animate({height:$ht})}
