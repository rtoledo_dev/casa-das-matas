module WelcomeHelper
  def big_banners
    Banner.bigs.limit(4)
  end

  def medium_banners
    Banner.mediums.limit(3)
  end
end
