class OrderHistory < ActiveRecord::Base
  belongs_to :order
  attr_accessible :informations, :order_id
  validates_presence_of :informations, :order_id

  def name
    return nil if self.new_record?
    "Criado em: #{I18n.l(self.created_at)} | Pedido: #{self.order.name rescue '-'}"
  end
end
