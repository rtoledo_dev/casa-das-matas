class ProductImage < ActiveRecord::Base
  belongs_to :product
  attr_accessible :image, :principal
  has_attached_file :image, styles: { thumb: "70x70#", normal: "300x259#", medium: '200x150#', big: '900x500#'},
                    default_style: :thumb
  validates :image, attachment_presence: true

  def name
    "#{self.image_file_name} | Produto: #{self.product.name rescue '-'}"
  end
end
