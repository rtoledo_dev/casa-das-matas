class Order < ActiveRecord::Base
  attr_accessible :client_email, :client_name, :finished, :total, :order_products_attributes
  attr_protected :total
  
  has_many :order_products, dependent: :destroy
  accepts_nested_attributes_for :order_products
  validates_presence_of :client_name, :client_email
  # has_many :order_histories, dependent: :destroy
  def name
    return nil if self.new_record?
    "#{self.client_name} #{I18n.l(self.created_at)} | #{self.total.to_f.real_contabil}"
  end
end
