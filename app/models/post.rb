class Post < ActiveRecord::Base
  belongs_to :category_post
  attr_accessible :content, :title, :category_post_id, :short_description
  validates :title, presence: true
  validates :short_description, presence: true
  validates :content, presence: true
  validates :category_post_id, presence: true
end
