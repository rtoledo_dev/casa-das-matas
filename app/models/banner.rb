# encoding: utf-8
class Banner < ActiveRecord::Base
  MEDIUM, BIG = 'Banner superior', 'Banner médio'
  scope :bigs, where(localization: BIG)
  scope :mediums, where(localization: MEDIUM)
  attr_accessible :description, :image, :position, :title, :localization, :url
   has_attached_file :image, styles: { thumb: "75x75#", normal: '300x300#', big: "940x500#", medium: "300x259#"},
                    default_style: :normal
  validates :image, attachment_presence: true
  validates_presence_of :title, :localization, :description, :url

  def localization_enum
    [Banner::BIG,Banner::MEDIUM]
  end
end
