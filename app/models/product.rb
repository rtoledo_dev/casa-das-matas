class Product < ActiveRecord::Base
  attr_accessible :description, :name, :price, :special_price, :weight, :short_description, 
  :product_images_attributes

  validates_presence_of :description, :name, :short_description
  validates_numericality_of :price, :weight, greater_than: 0
  validates_numericality_of :special_price, greater_than: 0, allow_nil: true

  has_many :product_images, dependent: :destroy
  accepts_nested_attributes_for :product_images

  def image
    self.product_images.order('principal DESC').first.image
  end
end
