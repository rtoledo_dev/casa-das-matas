# encoding: utf-8
# RailsAdmin config file. Generated on September 06, 2012 01:49
# See github.com/sferik/rails_admin for more informations

RailsAdmin.config do |config|

  # If your default_local is different from :en, uncomment the following 2 lines and set your default locale here:
  require 'i18n'
  I18n.default_locale = 'pt-BR'

  config.current_user_method { current_user } # auto-generated

  # If you want to track changes on your models:
  # config.audit_with :history, User

  # Or with a PaperTrail: (you need to install it first)
  # config.audit_with :paper_trail, User

  # Set the admin name here (optional second array element will appear in a beautiful RailsAdmin red ©)
  config.main_app_name = ['Café das Matas', 'Admin']
  # or for a dynamic name:
  # config.main_app_name = Proc.new { |controller| [Rails.application.engine_name.titleize, controller.params['action'].titleize] }


  #  ==> Global show view settings
  # Display empty fields in show views
  # config.compact_show_view = false

  #  ==> Global list view settings
  # Number of default rows per-page:
  # config.default_items_per_page = 20

  #  ==> Included models
  # Add all excluded models here:
  config.excluded_models = [Ckeditor::Asset, Ckeditor::AttachmentFile, Ckeditor::Picture]

  # Add models here if you want to go 'whitelist mode':
  # config.included_models = [Banner, CategoryPost, Order, OrderHistory, OrderProduct, Page, Parameter, Post, Product, ProductImage, User]

  # Application wide tried label methods for models' instances
  # config.label_methods << :description # Default is [:name, :title]

  #  ==> Global models configuration
  config.models do
  #   # Configuration here will affect all included models in all scopes, handle with care!
  #
    fields_of_type :text do
      ckeditor true
    end
  #   list do
  #     # Configuration here will affect all included models in list sections (same for show, export, edit, update, create)
  #
  #     fields_of_type :date do
  #       # Configuration here will affect all date fields, in the list section, for all included models. See README for a comprehensive type list.
  #     end
  #   end
  end
  #
  #  ==> Model specific configuration
  # Keep in mind that *all* configuration blocks are optional.
  # RailsAdmin will try his best to provide the best defaults for each section, for each field.
  # Try to override as few things as possible, in the most generic way. Try to avoid setting labels for models and attributes, use ActiveRecord I18n API instead.
  # Less code is better code!
  # config.model MyModel do
  #   # Cross-section field configuration
  #   object_label_method :name     # Name of the method called for pretty printing an *instance* of ModelName
  #   label 'My model'              # Name of ModelName (smartly defaults to ActiveRecord's I18n API)
  #   label_plural 'My models'      # Same, plural
  #   weight -1                     # Navigation priority. Bigger is higher.
  #   parent OtherModel             # Set parent model for navigation. MyModel will be nested below. OtherModel will be on first position of the dropdown
  #   navigation_label              # Sets dropdown entry's name in navigation. Only for parents!
  #   # Section specific configuration:
  #   list do
  #     filters [:id, :name]  # Array of field names which filters should be shown by default in the table header
  #     items_per_page 100    # Override default_items_per_page
  #     sort_by :id           # Sort column (default is primary key)
  #     sort_reverse true     # Sort direction (default is true for primary key, last created first)
  #     # Here goes the fields configuration for the list view
  #   end
  # end

  # Your model's configuration, to help you get started:

  # All fields marked as 'hidden' won't be shown anywhere in the rails_admin unless you mark them as visible. (visible(true))

  config.model Banner do
    label 'Banner'
    label_plural 'Banners'
    weight 0
  #   # Found associations:
  #   # Found columns:
  #     configure :id, :integer 
      # configure :image_file_name, :string         # Hidden 
      # configure :image_content_type, :string         # Hidden 
      # configure :image_file_size, :integer         # Hidden 
      # configure :image_updated_at, :datetime         # Hidden 
      configure :image, :paperclip 
      configure :title, :string 
      configure :description, :text 
      configure :url, :string
      configure :position, :integer 
      configure :localization
      configure :created_at, :datetime 
      configure :updated_at, :datetime   #   # Sections:
  #   list do; end
  #   export do; end
  #   show do; end
  #   edit do; end
  #   create do; end
  #   update do; end
  end
  config.model CategoryPost do
    label 'Categoria de post'
    label_plural 'Categorias de posts'
    weight 7
  #   # Found associations:
  #   # Found columns:
  #     configure :id, :integer 
      configure :name, :string 
  #     configure :created_at, :datetime 
  #     configure :updated_at, :datetime   #   # Sections:
  #   list do; end
  #   export do; end
  #   show do; end
  #   edit do; end
  #   create do; end
  #   update do; end
  end
  config.model Order do
    label 'Pedido'
    label_plural 'Pedidos'
    weight 1
  #   # Found associations:
  
  #     configure :id, :integer 
      configure :total
      configure :client_email, :string 
      configure :client_name, :string 
      configure :finished, :boolean 
      configure :order_products
      # configure :order_histories
      configure :created_at, :datetime 
      configure :updated_at, :datetime   #   # Sections:
  #   list do; end
  #   export do; end
  #   show do; end
  #   edit do; end
  #   create do; end
  #   update do; end
  end
  config.model OrderHistory do
    label 'Histórico de pedido'
    label_plural 'Históricos de pedidos'
    weight 3
  #   # Found associations:
      configure :order, :belongs_to_association   #   # Found columns:
  #     configure :id, :integer 
  #     configure :order_id, :integer         # Hidden 
      configure :informations, :text 
      configure :created_at, :datetime 
      configure :updated_at, :datetime   #   # Sections:
  #   list do; end
  #   export do; end
  #   show do; end
  #   edit do; end
  #   create do; end
  #   update do; end
  end
  config.model OrderProduct do
    label 'Item do pedido'
    label_plural 'Itens de pedidos'
    weight 2
  #   # Found associations:
      configure :order, :belongs_to_association do
        visible(false)
      end
      configure :product, :belongs_to_association   #   # Found columns:
  #     configure :id, :integer 
  #     configure :order_id, :integer         # Hidden 
  #     configure :product_id, :integer         # Hidden 
      configure :quantity, :integer 
      configure :price, :float 
  #     configure :created_at, :datetime 
  #     configure :updated_at, :datetime   #   # Sections:
  #   list do; end
  #   export do; end
  #   show do; end
  #   edit do; end
  #   create do; end
  #   update do; end
  end
  config.model Page do
    label 'Página'
    label_plural 'Páginas'
    weight 5
    # Found associations:
    # Found columns:
      # configure :id, :integer 
      configure :title, :string 
      configure :content, :text 
      configure :url, :string 
      configure :created_at, :datetime 
      configure :updated_at, :datetime   #   # Sections:
    list do; end
    export do; end
    show do; end
    edit do; end
    create do; end
    update do; end
  end
  config.model Parameter do
    label 'Parâmetros do sistema'
    label_plural 'Parâmetros do sistema'
    weight 8
  #   # Found associations:
  #   # Found columns:
  #     configure :id, :integer 
      configure :contact_to, :string 
      configure :pagseguro_token, :string 
      configure :pagseguro_email, :string 
      configure :created_at, :datetime 
      configure :updated_at, :datetime   #   # Sections:
  #   list do; end
  #   export do; end
  #   show do; end
  #   edit do; end
  #   create do; end
  #   update do; end
  end
  config.model Post do
    label 'Post do blog'
    label_plural 'Posts do blog'
    weight 6
  #   # Found associations:
      configure :category_post, :belongs_to_association   #   # Found columns:
  #     configure :id, :integer 
      # configure :category_post_id, :integer         # Hidden 
      configure :title, :string 
      configure :short_description, :string
      configure :content, :text 
      configure :created_at, :datetime 
      configure :updated_at, :datetime   #   # Sections:
  #   list do; end
  #   export do; end
  #   show do; end
  #   edit do; end
  #   create do; end
  #   update do; end
  end
  config.model Product do
    label 'Produto'
    label_plural 'Produtos'
    weight 4
  #   # Found associations:
      configure :product_images
  #     configure :id, :integer 
      configure :name, :string 
      configure :short_description, :string
      configure :description, :text
      configure :price, :float 
      configure :special_price, :float 
      configure :weight, :float 
  #     configure :created_at, :datetime 
  #     configure :updated_at, :datetime   #   # Sections:
  #   list do; end
  #   export do; end
  #   show do; end
  #   edit do; end
  #   create do; end
  #   update do; end
  end
  config.model ProductImage do
    label 'Imagem do produto'
    label_plural 'Imagens dos produtos'
    weight 5
  #   # Found associations:
      configure :product, :belongs_to_association do
        visible(false)
      end
      configure :image, :paperclip 
  #     configure :id, :integer 
  #     configure :product_id, :integer         # Hidden 
  #     configure :image_file_name, :string 
  #     configure :image_content_type, :string 
  #     configure :image_file_size, :integer 
  #     configure :image_updated_at, :datetime 
      configure :principal, :boolean 
  #     configure :created_at, :datetime 
  #     configure :updated_at, :datetime   #   # Sections:
  #   list do; end
  #   export do; end
  #   show do; end
  #   edit do; end
  #   create do; end
  #   update do; end
  end
  config.model User do
    label 'Usuário'
    label_plural 'Usuários'
    weight 9
  #   # Found associations:
  #   # Found columns:
  #     configure :id, :integer 
      configure :email, :string 
      configure :password, :password         # Hidden 
      configure :password_confirmation, :password         # Hidden 
      configure :reset_password_token, :string do
        visible(false)
      end
      configure :reset_password_sent_at, :datetime do
        visible(false)
      end
      configure :remember_created_at, :datetime do
        visible(false)
      end
      configure :sign_in_count, :integer do
        visible(false)
      end
      configure :current_sign_in_at, :datetime do
        visible(false)
      end
      configure :last_sign_in_at, :datetime do
        visible(false)
      end
      configure :current_sign_in_ip, :string do
        visible(false)
      end
      configure :last_sign_in_ip, :string do
        visible(false)
      end
      configure :created_at, :datetime 
      configure :updated_at, :datetime   #   # Sections:
  #   list do; end
  #   export do; end
  #   show do; end
  #   edit do; end
  #   create do; end
  #   update do; end
  end
end
