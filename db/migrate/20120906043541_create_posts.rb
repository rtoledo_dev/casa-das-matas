class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.references :category_post
      t.string :title
      t.text :content

      t.timestamps
    end
    add_index :posts, :category_post_id
  end
end
