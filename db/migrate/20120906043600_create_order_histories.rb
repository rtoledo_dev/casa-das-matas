class CreateOrderHistories < ActiveRecord::Migration
  def change
    create_table :order_histories do |t|
      t.references :order
      t.text :informations

      t.timestamps
    end
    add_index :order_histories, :order_id
  end
end
