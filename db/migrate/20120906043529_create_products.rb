class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.text :description
      t.float :price
      t.float :special_price
      t.float :weight

      t.timestamps
    end
  end
end
