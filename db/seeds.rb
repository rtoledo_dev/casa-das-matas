# encoding: utf-8

10.times do |i|
    Post.new(
    title: "Noticia #{i}", 
    content: 'Est omnis doloresereledusempquibdameto aure rumoms odes necessitatibus saepem eveniet estude.susandaeItaquearumumel.', 
    short_description: 'Est omnis doloresereledusempquibdameto aure '
    ).save(validate: false)
end

%w(page1-img1.jpg page1-img2.jpg page1-img3.jpg).each_with_index do |t,i|
  Banner.mediums.new(
    title: "Médio Banner #{i}", 
    description: 'Est omnis doloresereledusempquibdameto aure rumoms odes necessitatibus saepem eveniet estude.susandaeItaquearumumel.', 
    url: '#',
    position: i+1,
    image: File.open(File.join(Rails.root,'db','images','banners',t))
    ).save(validate: false)

  3.times do |j|
    product = Product.new(
        name: "Produto #{i}", 
        description: 'Est omnis doloresereledusempquibdameto aure rumoms odes necessitatibus saepem eveniet estude.susandaeItaquearumumel.', 
        short_description: 'Est omnis doloresereledusempquibdameto aure ', 
        price: 99,
        weight: 1
        )
    product.save(validate: false )
    product.product_images.build(
        image: File.open(File.join(Rails.root,'db','images','banners',t))
        ).save(validate: false )

    end
end

%w(slide-1.jpg slide-2.jpg slide-3.jpg slide-4.jpg).each_with_index do |t,i|
  Banner.bigs.new(
    title: "Grande Banner #{i}", 
    description: 'Est omnis doloresereledusempquibdameto aure rumoms odes necessitatibus saepem', 
    url: '#',
    image: File.open(File.join(Rails.root,'db','images','banners',t))
    ).save(validate: false)
end